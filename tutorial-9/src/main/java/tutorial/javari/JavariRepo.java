package tutorial.javari;


import org.springframework.stereotype.Repository;
import tutorial.javari.animal.Animal;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;

import java.io.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


@Repository
public class JavariRepo {
    private final String fileName = "./tutorial-9/src/main/java/tutorial/javari/animals_records.csv";
    private List<Animal> map = new ArrayList<>();
    private boolean accesed = false;

    public void setAccesed(boolean accesed) {
        this.accesed = accesed;
    }

    public List<Animal> getMap() {
        return map;
    }

    public void addAnimal(Animal animal) {
        map.add(animal);
    }

    public boolean isAccesed() {
        return accesed;
    }

    public void readCSVfirstTime() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        String line;



        while((line= br.readLine()) != null) {
            String str[] = line.split(",");
            map.add(new Animal(Integer.parseInt(str[0]), str[1], str[2], Gender.parseGender(str[3]),
                    Double.parseDouble(str[4]), Double.parseDouble(str[5]), Condition.parseCondition(str[6])));
        }

        br.close();
    }

    public void writeCSV() throws IOException {
        FileWriter writer = new FileWriter(fileName, false);


        for(Animal animal : map) {
            //System.out.println(animal.getId());
            writer.write(String.valueOf(animal.getId()));
            writer.write(",");
            writer.write(animal.getType());
            writer.write(",");
            writer.write(animal.getName());
            writer.write(",");
            writer.write(animal.getGender().toString().toLowerCase());
            writer.write(",");
            writer.write(String.valueOf(animal.getLength()));
            writer.write(",");
            writer.write(String.valueOf(animal.getWeight()));
            writer.write(",");
            writer.write(animal.getCondition().toString().toLowerCase());
            writer.write("\n");

        }
        writer.close();

    }

}
