package game;

import java.util.concurrent.atomic.AtomicInteger;

public class Timer implements Runnable{
    private AtomicInteger timer;
    private AtomicInteger threshold;

    public Timer(){
        this.timer = new AtomicInteger(0);
        this.threshold = new AtomicInteger(0);
    }
    @Override
    public void run() {
        while(true) {
            try {
                Thread.sleep(1000);
                this.timer.getAndIncrement();
                this.threshold.getAndIncrement();
            } catch (InterruptedException e) {
                e.getStackTrace();
                }
            }
        }

    public AtomicInteger getTimer() {
        return this.timer;
    }

    public AtomicInteger getThreshold() {
        return this.threshold;
    }

    public int calculateInsideThreshold(int points) {
        return (int) (points * 0.1);
    }

    public int calculateOutsideThreshold(int points) {
        return (int) (points * 0.05);
    }


}
