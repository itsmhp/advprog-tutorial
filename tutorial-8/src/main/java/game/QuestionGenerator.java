package game;

import java.util.Random;

public class QuestionGenerator implements Runnable {
    private static final int QUEST_TYPE_ADD = 0;
    private static final int QUEST_TYPE_SUBSTR = 1;
    private static final int QUEST_TYPE_MULTIPL = 2;
    private static final int QUEST_TYPE_DIVS = 3;
    private static Fraction expectedAnswer;
    @Override
    public void run() {
        Random random = new Random();
        Fraction firstPosFrac = new Fraction(random.nextInt(40) - 20,
                random.nextInt(40) - 20);
        Fraction secondPosFrac = new Fraction(random.nextInt(40) - 20,
                random.nextInt(40) - 20);

        switch (random.nextInt(3)) {
            case QUEST_TYPE_ADD:
                System.out.print(firstPosFrac.toString() + "  +  "
                        + secondPosFrac.toString() + "  =  ");
                expectedAnswer = firstPosFrac.getAddition(secondPosFrac);
                break;
            case QUEST_TYPE_SUBSTR:
                System.out.print(firstPosFrac.toString() + "  -  "
                        + secondPosFrac.toString() + "  =  ");
                expectedAnswer = firstPosFrac.getSubstraction(secondPosFrac);
                break;
            case QUEST_TYPE_MULTIPL:
                System.out.print(firstPosFrac.toString() + "  *  "
                        + secondPosFrac.toString() + "  =  ");
                expectedAnswer = firstPosFrac.getMultiplication(secondPosFrac);
                break;
            case QUEST_TYPE_DIVS:
                System.out.print(firstPosFrac.toString() + "  :  "
                        + secondPosFrac.toString() + "  =  ");
                expectedAnswer = firstPosFrac.getDivision(secondPosFrac);
                break;
            default:
                System.out.println("Oooops!");
                expectedAnswer = new Fraction();
        }
    }

    public Fraction getExpectedAnswer() {
        return expectedAnswer;
    }
}
