import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class CustomerTest {
    private Movie movie1;
    private Rental rent1;

    private Movie movie2;
    private Rental rent2;

    private Customer customer;

    @Before
    public void setUp() {
        movie1 = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        rent1 = new Rental(movie1, 3);

        movie2 = new Movie("Interstellar", Movie.NEW_RELEASE);
        rent2 = new Rental(movie2, 2);

        customer = new Customer("Alice");

        customer.addRental(rent1);
    }

    @Test
    public void getName() {
        assertEquals("Alice", customer.getName());
    }

    @Test
    public void statementWithSingleMovie() {
        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    @Test
    public void statementWithMultipleMovies() {
        customer.addRental(rent2);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(5, lines.length);
        assertTrue(result.contains("Amount owed is 9.5"));
        assertTrue(result.contains("3 frequent renter points"));
    }

    @Test
    public void htmlStatementWithSingleMovie() {
        String result = customer.htmlStatement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("<P> you owe <EM>3.5</EM><P>"));
        assertTrue(result.contains("On this rental you earned <EM>1</EM>"
                                    + " frequent renter points<P>"));
    }

    @Test
    public void htmlStatementWithMultipleMovies() {
        customer.addRental(rent2);

        String result = customer.htmlStatement();
        String[] lines = result.split("\n");

        assertEquals(5, lines.length);
        assertTrue(result.contains("<P> you owe <EM>9.5</EM><P>"));
        assertTrue(result.contains("On this rental you earned <EM>3</EM>"
                                    + " frequent renter points<P>"));
    }
}