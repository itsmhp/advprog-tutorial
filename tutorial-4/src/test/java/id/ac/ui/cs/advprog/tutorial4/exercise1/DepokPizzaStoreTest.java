package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.SpecialPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza;
import org.junit.Before;
import org.junit.Test;

public class DepokPizzaStoreTest {
    private DepokPizzaStore store;
    private Pizza pizza;

    @Before
    public void setUp() {
        store = new DepokPizzaStore();
    }

    @Test
    public void testVeggiePizza() {
        pizza = store.orderPizza("veggie");
        assertTrue(pizza instanceof VeggiePizza);
        assertEquals("Depok Style Veggie Pizza", pizza.getName());
    }

    @Test
    public void testCheesePizza() {
        pizza = store.orderPizza("cheese");
        assertTrue(pizza instanceof CheesePizza);
        assertEquals("Depok Style Cheese Pizza", pizza.getName());
    }

    @Test
    public void testClamPizza() {
        pizza = store.orderPizza("clam");
        assertTrue(pizza instanceof ClamPizza);
        assertEquals("Depok Style Clam Pizza", pizza.getName());
    }

    @Test
    public void testSpecialPizza() {
        pizza = store.orderPizza("special");
        assertTrue(pizza instanceof SpecialPizza);
        assertEquals("Depok's Special All-Ingredients Pizza", pizza.getName());
    }
}
