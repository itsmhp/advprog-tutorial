package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class Scallop implements Clams {
    public String toString() {
        return "Scallop from Madagascar";
    }
}
