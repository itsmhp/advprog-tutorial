package matrix;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MatrixMultiplicationTest {
    //TODO Implement, apply your test cases here
    private static String path = "plainTextDirectory/input/matrixProblem";
    private static String pathFile1 = path + "A/matrixProblemSet1.txt";
    private static String pathFile2 = path + "A/matrixProblemSet2.txt";


    private double[][] squareMatrix;
    private double[][] matrix1Tester;
    private double[][] matrix2Tester;

    @Before
    public void setUp() throws IOException {
        squareMatrix = new double[][]{{45, 67, 89, 100, 111}, {112, 56, 66, 88, 32}};
        matrix1Tester = Main.convertInputFileToMatrix(pathFile1, 50, 50);
        matrix2Tester = Main.convertInputFileToMatrix(pathFile2, 50, 50);
    }

    @Test
    public void testIllegalMultiply() {
        boolean isIllegal = false;
        try {
            MatrixOperation.basicMultiplicationAlgorithm(squareMatrix, squareMatrix);
        } catch (InvalidMatrixSizeForMultiplicationException e) {
            isIllegal = true;
        } finally {
            Assert.assertTrue(isIllegal);
        }
    }

    @Test
    public void testBenchMark() throws InvalidMatrixSizeForMultiplicationException {
        for (int i = 0; i < 100; i++) {
            MatrixOperation.basicMultiplicationAlgorithm(matrix1Tester, matrix2Tester);
        }
        long timeforNBasicMultiply = System.nanoTime();
        for (int j = 0; j < 1000; j++) {
            MatrixOperation.basicMultiplicationAlgorithm(matrix1Tester, matrix2Tester);
        }
        timeforNBasicMultiply = (System.nanoTime() - timeforNBasicMultiply) / 1000;

        for (int i = 0; i < 100; i++) {
            MatrixOperation.strassenMatrixMultiForNonSquareMatrix(matrix1Tester, matrix2Tester);
        }
        long timeforStrassenMultiply = System.nanoTime();
        for (int j = 0; j < 1000; j++) {
            MatrixOperation.strassenMatrixMultiForNonSquareMatrix(matrix1Tester, matrix2Tester);
        }
        timeforStrassenMultiply = (System.nanoTime() - timeforStrassenMultiply) / 1000;
        System.out.println("Benchmark Report : Basic Operation");
        System.out.println("Warmup Iterations : 100 times");
        System.out.println("Testing iterations : 1000 times");
        System.out.println("Average execution time : " + timeforNBasicMultiply + " ns");
        System.out.println();
        System.out.println("Benchmark Report : Strassen Operation");
        System.out.println("Warmup Iterations : 100 times");
        System.out.println("Testing iterations : 1000 times");
        System.out.println("Average execution time : " + timeforStrassenMultiply + " ns");
    }
}
